import math

from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers.convolutional import Conv3D, MaxPooling3D, Conv2D
from tensorflow.python.keras.layers.core import Dense
from tensorflow.python.keras.layers.convolutional_recurrent import ConvLSTM2D
from tensorflow.python.keras.layers.normalization import BatchNormalization
import numpy as np
#import pylab as plt
import os

os.environ["CUDA_VISIBLE_DEVICES"] = "0"
data = np.load('fac_cond256_unit8.npz')
origin_X = data['data']
origin_Y = data['fac']
raw_Y = origin_Y.astype(np.float16)
raw_X = np.expand_dims(origin_X, -1)
raw_Y = np.expand_dims(raw_Y, 1)
raw_Y = np.expand_dims(raw_Y, 1)

Xtrain = raw_X[:-1000]
Ytrain = raw_Y[:-1000]
Xtest = raw_X[-1000:]
Ytest = raw_Y[-1000:]
print(Xtrain.shape, Ytest.shape)
seq = Sequential()
seq.add(ConvLSTM2D(filters=16, kernel_size=(3, 3),
                   input_shape=(None, 256, 256, 1),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=32, kernel_size=(3, 3),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=32, kernel_size=(3, 3),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=32, kernel_size=(3, 3),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=64, kernel_size=(3, 3),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=32, kernel_size=(3, 3),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=16, kernel_size=(3, 3),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=8, kernel_size=(3, 3),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=3, kernel_size=(3, 3),
                   padding='same', return_sequences=False))
seq.add(BatchNormalization())

# seq.add(Conv2D(filters=3, kernel_size=(3, 3),
#                activation='sigmoid',
#                padding='same', data_format='channels_last'))
seq.add(Dense(3))

seq.compile(loss="mean_squared_error", optimizer='adam', metrics=["mean_squared_error"])
seq.summary()
BATCH_SIZE = 1
NUM_EPOCHS = 5
seq.fit(Xtrain, Ytrain, epochs=NUM_EPOCHS, batch_size=BATCH_SIZE, validation_data=(Xtest, Ytest), shuffle=True)

seq.save('model.h5')

score, _ = seq.evaluate(Xtest, Ytest, batch_size=BATCH_SIZE)
rmse = math.sqrt(score)
print("\nMSE: {:.3f}, RMSE: {:.3f}".format(score, rmse))

pre = seq.predict(Xtest, batch_size=BATCH_SIZE)
np.save('pre.npy', pre)
