#!/usr/bin/python3.8
# -*- coding: utf-8 -*-
# @Time    : 2021/6/28 上午9:57
# @Author  : hxg
# @File    : ConvLSTM.py
# @Project: Lyu

from keras.models import Sequential
from keras.layers.convolutional import Conv3D, MaxPooling3D, Conv2D
from keras.layers.convolutional_recurrent import ConvLSTM2D
from keras.layers.normalization import BatchNormalization
import numpy as np
import pylab as plt

seq = Sequential()
seq.add(ConvLSTM2D(filters=30, kernel_size=(3, 3),
                   input_shape=(None, 256, 256, 3),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=30, kernel_size=(3, 3),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=30, kernel_size=(3, 3),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=30, kernel_size=(3, 3),
                   padding='same', return_sequences=True))
seq.add(BatchNormalization())

seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
seq.add(ConvLSTM2D(filters=30, kernel_size=(3, 3),
                   padding='same', return_sequences=False))
seq.add(BatchNormalization())

# seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
# seq.add(ConvLSTM2D(filters=30, kernel_size=(3, 3),
#                    padding='same', return_sequences=False))
# seq.add(BatchNormalization())
#
# seq.add(MaxPooling3D(pool_size=(1, 2, 2), strides=None, padding='valid', data_format=None))
# seq.add(ConvLSTM2D(filters=30, kernel_size=(3, 3),
#                    padding='same', return_sequences=False))
# seq.add(BatchNormalization())

seq.add(Conv2D(filters=3, kernel_size=(3, 3),
               activation='sigmoid',
               padding='same', data_format='channels_last'))
seq.compile(loss="mean_squared_error", optimizer='adam', metrics=["mean_squared_error"])
seq.summary()
